// 
// db.products.insertMany(
//     [
//         {
//             "name": "Iphone X",
//             "price": 30000,
//             "isActive": true
//         },
//         {
//             "name": "Samsung Galaxy s21",
//             "price": 51000,
//             "isActive": true
//         },
//         {
//             "name": "Razer Blackshark V2X",
//             "price": 2800,
//             "isActive": true
//         },
//         {
//             "name": "RAKK Gaming Mouse",
//             "price": 1800,
//             "isActive": true
//         },
//         {
//             "name": "SRazer MEchanical Keyboard",
//             "price": 4000,
//             "isActive": true
//         }
// 
//     ]
// )
// 

//Query operators - allows us to expand our queries and define conditions 
//instead of just looking specific values


//$gt,$lt,$gte,$lte


//$gt - query operator which means greater than
// db.products.find ({price:{$gt: 3000}})



//$lt - query operator which means less than
// db.products.find ({price:{$lt:3000}})


//$gte- query operator which means greater than or equal
// db.products.find ({price:{$gte:30000}})


//$lte- query operator which means less than or equal
// db.products.find ({price:{$lte:2800}})

// 
// db.users.insertMany(
//  [ 
//     {
//      "firstname": "Mary Jane",
//      "lastname": "Watson",
//      "email": "mjtiger@gmail.com",
//      "password": "tigerjackpot15",
//      "isAdmin": false   
//     },
//      {
//      "firstname": "Gwen",
//      "lastname": "Stacy",
//      "email": "stacyTech@gmail.com",
//      "password": "stacyTech1991",
//      "isAdmin": true  
//     },
//     {
//      "firstname": "Peter",
//      "lastname": "Parker",
//      "email": "peterWebDevh@gmail.com",
//      "password": "webdeveloperPeter",
//      "isAdmin": true  
//     },
//     {
//      "firstname": "Jonah",
//      "lastname": "JAmeson",
//      "email": "jjjameson@gmail.com",
//      "password": "spideyisamenace",
//      "isAdmin": false
//     },
//     {
//      "firstname": "Otto",
//      "lastname": "Octavius",
//      "email": "jottoOctopi@gmail.com",
//      "password": "docOck15",
//      "isAdmin": true
//     }
// 
//   ]
// )
// 
// 


//$regex - query operator which will allow us to find documents which will match the 
//characters/pattern of the characters we are looking for.
//case sensitive

db.users.find({firstname:{$regex:'O'}})


//$options - used out of regex will be case insensitive
db.users.find({firstname:{$regex:'o', $options: '$i'}})


//you can also find for documents with partial matches
db.products.find({name: {$regex: 'phone', $options: '$i'}}) //Iphone

db.users.find({email: {$regex: 'web', $options: '$i'}}) //peter parker


db.products.find({name: {$regex: 'razer', $options: '$i'}}) 
db.products.find({name: {$regex: 'rakk', $options: '$i'}}) 

//$or $and - logical operators 
// $or - allow us to have a logical operations to find for documents which
// can satisfy at least 1 of our conditions 1 true = all true 


db.products.find({$or:[{name: {$regex: 'x', $options: '$i'}}, {price: {$lte: 10000}}]})
db.products.find({$or:[{name: {$regex: 'x', $options: '$i'}}, {price: {$gte: 30000}}]})


//$and - look or find for documents that satisfy both or all conditions:

db.products.find({$and:[{name: {$regex: 'razer', $options: '$i'}}, {price: {$gte: 3000}}]})
db.products.find({$and:[{name: {$regex: 'x', $options: '$i'}}, {price: {$gte: 3000}}]})

db.users.find({$and:[{lastname: {$regex: 'w', $options: '$i'}}, {isAdmin: false}]})

db.users.find({$and:[{firstname: {$regex: 'g', $options: '$i'}}, {isAdmin: true}]})


//Field projection - will allow us to show or hide certain property or field
//db.collection.find({query},{projection}): 0-means hide 1-means show

db.users.find({ },{_id:0,password:0})

db.users.find({isAdmin:true},{_id:0,email:1}) //specifying what you only want to show


//id field must be explicitly hidden. We can also just pick which fields to show:
db.users.find({isAdmin:false},{firstname:1,lastname:1}) 

db.products.find({price:{$gte:10000}},{_id:0,name:1,price:1}) 



// db.courses.insertMany(
//   [
//     {
//      "name": "HTML Basics",
//      "price": 20000,
//      "isActive": true,
//      "instructor": "Sir Alvin"
//     },
//     {
//      "name": "CSS 101 + Flexbox",
//      "price": 21000,
//      "isActive": true,
//      "instructor": "Sir Alvin"
//     },
//     {
//      "name": "Javascript 101",
//      "price": 32000,
//      "isActive": true,
//      "instructor": "Ma'am Tine"
//     },
//     {
//      "name": "Git 101, IDE and CLI",
//      "price": 19000,
//      "isActive": false,
//      "instructor": "Ma'am Tine"
//     },
//     {
//      "name": "React.Js 101",
//      "price": 25000,
//      "isActive": true,
//      "instructor": "Ma'am TMiah"
//     }
// 
//   ]
// )
// 
db.courses.find({instructor:"Sir Alvin"},{_id:0,name:1,price:1},{price: {$gte: 20000}}) 

db.courses.find({instructor:"Ma'am Tine"},{_id:0,name:1,price:1},{isActive:false}) 

db.courses.find({$and:[{name: {$regex: 'r', $options: '$i'}}, {price: {$lte: 25000}}]})

db.courses.updateOne({"price": "inactive"}, {$set: {"price":19000}})

db.courses.deleteMany({price: {$gte: 25000}})

